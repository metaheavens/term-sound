{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

import Graphics.Vty

import Control.Applicative hiding ((<|>))
import Control.Arrow hiding (loop)
import Control.Monad.RWS
import Control.Monad.State
import Data.Int (Int32, )
import Data.Sequence (Seq, (<|) )
import qualified Data.Sequence as Seq
import Data.Foldable
import System.Random
import Control.Concurrent.STM.TMQueue
import Control.Concurrent.Chan
import Sound.ALSA.PCM
         (SoundFmt(SoundFmt), sampleFreq, soundSourceRead,
          SoundSource, alsaSoundSource, withSoundSource, )
import GHC.Conc
import Foreign
import Data.Int (Int16, )
import Control.Concurrent.Async
import Data.Vector hiding (map, replicate, length, zip, update, (++))
import Numeric.FFT.Vector.Unitary
import Debug.Trace
import System.Exit
import Control.Exception
import Say

type App = StateT Vty IO

bufSize :: Int
bufSize = 1024
sampleF = 8000
maxFFFT :: Double = fromIntegral $ sampleF `div` 2

inputFormat :: SoundFmt Int16
inputFormat = SoundFmt { sampleFreq = sampleF }

usefulSize = bufSize `div` 2 - 1

hasLocked :: String -> IO a -> IO a
hasLocked msg action =
  action `catches`
  [Handler $ \exc@BlockedIndefinitelyOnSTM -> sayString ("[STM]: " ++ msg) >> throwIO exc]

main = do
    vty <- mkVty defaultConfig
    (soundChan, thread) <- getSoundChan
    execStateT (loop soundChan) vty
    wait thread
    shutdown vty

liftStateTVector :: IO (Maybe (Maybe (Vector Double))) -> StateT Vty IO (Maybe (Maybe (Vector Double)))
liftStateTVector io = lift io

-- | assumes that the file contains numbers in the host's byte order
loop :: TMQueue (Vector Double) -> App ()
loop soundChan = do
    let errorCheck = hasLocked "It sucks ! 2"
    sound <- liftStateTVector $ errorCheck $ atomically $ tryReadTMQueue soundChan
    case sound of
        Just sound -> do
            case sound of
                Just s -> do
                    let fft :: Vector Double = run dct2 s
                    let fft_res = map (\x -> round $ x :: Int) $ Data.Vector.toList fft
                    let uniformized = uniformize 400 fft_res
                    updateDisplay $ colorbox_240 50 uniformized
                    loop soundChan
                Nothing -> do
                    loop soundChan
        Nothing -> do
            _ <- lift $ print "EXIT FAILURE"
            lift exitFailure

uniformize :: Int -> [Int] -> [Int]
uniformize nbBar fft =
    let eMax = logBase (fromIntegral 10) maxFFFT in
    let eSplits = [10 ** (eMax * (fromIntegral x) / (fromIntegral nbBar)) | x <- [0..nbBar]] in
    let nSplit = [x * (fromIntegral bufSize) / maxFFFT | x <- eSplits] in
    let zipped = Prelude.take (length nSplit) (zip (0 : nSplit) (nSplit Prelude.++ [0])) in
    let splits = [(fst $ Prelude.splitAt ((round to :: Int) - (round from :: Int)) (snd $ Prelude.splitAt (round from :: Int) fft)) | (from, to) <- zipped] in
    [Data.Foldable.maximum x | x <- splits, not (Data.Foldable.null x)]

colorbox_240 :: Int -> [Int] -> Image
colorbox_240 max list = horizCat $ (map (\i -> columnBlock max i $ colorBlock 239) list)

colorBlock i = string (currentAttr `withBackColor` Color240 i) " "

defaultBlock :: Image
defaultBlock = string defAttr " "

columnBlock :: Int -> Int -> Image -> Image
columnBlock max height block = vertCat [if x > max - height then block else defaultBlock | x <- [0..max]]

updateDisplay :: Image -> App ()
updateDisplay image = do
    let info = string (defAttr `withForeColor` black `withBackColor` green) "Press ESC to exit. Events for keys below."
    let pic = picForImage (info <-> image)
    vty <- get
    liftIO $ update vty pic

getSoundChan :: IO (TMQueue (Vector Double), Async ())
getSoundChan = do
    chan <- atomically newTMQueue
    thread <- async $ allocaArray bufSize $ \buf -> do
        let source = alsaSoundSource "default" inputFormat
        withSoundSource source $ \from ->
             loopSound source from chan buf
    return (chan, thread)

loopSound :: SoundSource h Int16 -> h Int16 -> TMQueue (Vector Double) -> Ptr Int16 -> IO ()
loopSound source from chan buf = do
    soundSourceRead source from (castPtr buf) bufSize
    xs <- peekArray bufSize buf
    let converted = Data.Vector.take usefulSize $ fromList [fromIntegral $ div x 700 | x <- xs]
    let errorCheck = hasLocked "It sucks !"
    errorCheck $ atomically $ writeTMQueue chan converted
    loopSound source from chan buf
